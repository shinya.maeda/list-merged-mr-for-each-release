require 'gitlab'
require 'uri'

EE_REPO = '/Users/shinyamaeda/Documents/gitlab/gdk-ee/gitlab'
FROM_TAG = 'v10.5.3-ee'
TO_TAG = 'v10.6.0-rc1-ee'
CATEGORIES = %w[CI/CD Documentation]
CATEGORY_OTHERS = 'Others'

Gitlab.configure do |config|
  config.endpoint       = 'https://gitlab.com/api/v4' # API endpoint URL, default: ENV['GITLAB_API_ENDPOINT']
  config.private_token  = ''       # user's private token or OAuth2 access token, default: ENV['GITLAB_API_PRIVATE_TOKEN']
end

cmd = "git -C #{EE_REPO} log #{FROM_TAG}..#{TO_TAG} | grep 'See merge request'"
value = `#{cmd}`

results = {}

value.each_line do |line|
  ret = line.scan(/See merge request (.*)\!(\d+)/).last
  namespace_project = ret[0]
  mr_id = ret[1]

  begin
    mr = Gitlab.merge_request(URI.escape(namespace_project), mr_id.to_i)

    is_added = false
    CATEGORIES.each do |cat|
      if mr.labels.include?(cat)
        results[cat] ||= []
        results[cat] << mr
        is_added = true
        break
      end
    end

    unless is_added
      results[CATEGORY_OTHERS] ||= []
      results[CATEGORY_OTHERS] << mr
    end
  rescue => e
    puts "ERRRO: #{e.message}"
  end
end

results.each do |key, value|
  puts "### Category: #{key}"
  value.each do |mr|
    puts "- [#{mr.title}](#{mr.web_url}) Labels: #{mr.labels.map!{ |label| "~#{label}" }.join(' ')}"
  end
  puts ""
end
